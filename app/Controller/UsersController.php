<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 */
class UsersController extends AppController {

/**
 * overwrite the beforeFilter callback of AppController
 */
	public function beforeFilter() {
		parent::beforeFilter();
	}

/**
 * admin_login method
 *
 * @return void
 */
	public function admin_login() {
		$this->layout = 'admin-login';
		if ($this->request->is('post')) {
			debug($this->request->data);
		}
	}

/**
 * admin_dashboard method
 *
 * @return void
 */
	public function admin_dashboard() {
		
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->User->recursive = 0;
		$this->set('sideSection', 'index');
		$this->set('title_for_layout', 'User Index');	
		$this->set('users', $this->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		$this->set('user', $this->User->read(null, $id));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->User->create();
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->request->is('post') || $this->request->is('put')) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The user has been saved'));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The user could not be saved. Please, try again.'));
			}
		} else {
			$this->request->data = $this->User->read(null, $id);
		}
	}

/**
 * admin_delete method
 *
 * @throws MethodNotAllowedException
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		if (!$this->request->is('post')) {
			throw new MethodNotAllowedException();
		}
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid user'));
		}
		if ($this->User->delete()) {
			$this->Session->setFlash(__('User deleted'));
			$this->redirect(array('action' => 'index'));
		}
		$this->Session->setFlash(__('User was not deleted'));
		$this->redirect(array('action' => 'index'));
	}

/**
 * function to return filterActions for index view
 */
	public function admin_index_filters() {
		return array(
			'fields' => array(
				'User.username' => array(
					'placeholder' => 'Username'
				),
				'User.first_name' => array(
					'placeholder' => 'First Name',
				)
			)
		);
	}

	public function admin_quick_links() {
		return array(
			'sections' => array(
				__('Users') => array(
					__('List all Users') => array(
						'controller' => 'users',
						'action' => 'index'
					),
					__('Add new User') => array(
						'controller' => 'users',
						'action' => 'add'
					)
				)
			)
		);
	}
}
